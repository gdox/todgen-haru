//
// image.rs
// Copyright (C) 2019 gerwin <gerwin@gerwin-linux>
// Distributed under terms of the MIT license.
//


use crate::Error;

use image;

use std::io::Write;
use std::path::Path;


pub struct Image {
    pub image_data : Vec<u8>,
}

impl Image {
    pub fn from_file<P : AsRef<Path> + ?Sized>(filename : &P) -> Result<Image, Error> {
        Self::from_file_inner(filename.as_ref())
    }

    fn from_file_inner(filename : &Path) -> Result<Image, Error> {
        let img = image::open(filename)?;
        let mut res = Image {image_data : Vec::new()};
        img.write_to(&mut res.image_data, image::ImageFormat::PNG)?;
        Ok(res)
    }
}


#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn it_works() {
	}
}
