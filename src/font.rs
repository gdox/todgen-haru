//
// font.rs
// Copyright (C) 2019 gerwin <gerwin@gerwin-linux>
// Distributed under terms of the MIT license.
//


pub enum Font {
	Builtin(u8),
	Custom,
}


#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn it_works() {
	}
}
