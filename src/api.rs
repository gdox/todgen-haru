//
// api.rs
// Copyright (C) 2019 gerwin <gerwin@gerwin-linux>
// Distributed under terms of the MIT license.
//

pub use crate::shapes::{Rectangle, Dots};

#[derive(Debug, Clone, Copy)]
pub enum Color {
	None,
	RGBA([u8; 4]),
}

pub use crate::image::Image;
pub use crate::font::Font;

pub trait PageAPI : Sized {
	type Error;
	type Document;
	fn new_page(document : Self::Document, rectangle : Rectangle) -> Result<Self, Self::Error>;
	fn set_line(&mut self, color : &Color) -> Result<(), Self::Error>;
	fn set_fill(&mut self, color : &Color) -> Result<(), Self::Error>;
	fn draw_text<'b>(&mut self, text : &'b str, rectangle : &mut Rectangle) -> Result<&'b str, Self::Error>;
	fn draw_rectangle(&mut self, rectangle : &Rectangle) -> Result<(), Self::Error>;
	fn draw_image(&mut self, rectangle : &Rectangle, image : &Image) -> Result<(), Self::Error>;
}


#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn it_works() {
	}
}
