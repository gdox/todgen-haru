//
// haru.rs
// Copyright (C) 2019 gerwin <gerwin@gerwin-linux>
// Distributed under terms of the MIT license.
//


use crate::PageAPI;
use crate::Color;
use crate::Error;
use crate::Image;

use libharu_sys::*;
use std::ptr;
use std::ffi;

use crate::shapes::{Rectangle, Dots};

const BUFFER_LEN : usize = 4096;

#[no_mangle]
extern "C" fn error_handler(
        error_no: HPDF_STATUS,
        detail_no: HPDF_STATUS,
        user_data: *mut ::std::os::raw::c_void,
) {
    eprintln!("Error happened: {} {}", error_no, detail_no);
}
trait Pointer : Sized {
    fn denull(self) -> Result<Self, Error>;
}

impl<T> Pointer for *mut T {
    fn denull(self) -> Result<Self, Error> {
        if self.is_null() {
            Err("Pointer is null".into())
        } else {
            Ok(self)
        }
    }
}

impl<T> Pointer for *const T {
    fn denull(self) -> Result<Self, Error> {
        if self.is_null() {
            Err("Pointer is null".into())
        } else {
            Ok(self)
        }
    }
}

impl Pointer for HPDF_STATUS {
    fn denull(self) -> Result<Self, Error> {
        if self == HPDF_OK as _ {
            Ok(self)
        } else {
            Err(format!("Haru Error Code {}", self).into())
        }
    }
}

pub struct HaruDocument {
    handle : HPDF_Doc,
} 

impl HaruDocument {
    pub fn new() -> Result<HaruDocument, Error> {
        let handle = unsafe {HPDF_New(Some(error_handler), ptr::null_mut())}.denull()?;
        Ok(HaruDocument { handle })
    }

    pub fn save(self) -> Result<Vec<u8>, Error> {
        let mut res = Vec::new();
        unsafe {
            HPDF_SaveToStream (self.handle).denull()?;

            let mut buffer = [0;BUFFER_LEN];
            loop {
                let mut i : u32 = BUFFER_LEN as _;
                let err = HPDF_ReadFromStream(self.handle, &mut buffer[0], &mut i);
                if err == HPDF_STREAM_EOF as _ || err == HPDF_OK as _ {
                    res.extend_from_slice(&buffer[..i as usize]);
                    if err == HPDF_STREAM_EOF as _ {
                        return Ok(res);
                    }
                }
                err.denull()?;
            }

        }
    }
}

impl Drop for HaruDocument {
    fn drop(&mut self) {
        unsafe {HPDF_Free(self.handle)};
    }
}

pub struct HaruConfig {
    text_leading : HPDF_REAL,
    text_size : HPDF_REAL,
}

impl Default for HaruConfig {
    fn default() -> Self {
        HaruConfig {
            text_leading : 20.,
            text_size : 13.,
        }
    }
}
pub struct HaruPage<'a> {
    doc : &'a mut HaruDocument,
    page : HPDF_Page,
    offset : Rectangle,
    config : HaruConfig,
}

impl<'a> HaruPage<'a> {
    fn to_page_x(&self, x : Dots) -> HPDF_REAL {
        (x - self.offset.left) as HPDF_REAL
    }
    fn to_page_y(&self, y : Dots) -> HPDF_REAL {
        (self.offset.bottom.unwrap() - y) as HPDF_REAL
    }
    fn from_page_x(&self, x : HPDF_REAL) -> Dots {
        x as Dots + self.offset.left
    }
    fn from_page_y(&self, y : HPDF_REAL) -> Dots {
        self.offset.bottom.unwrap() - (y as Dots)
    }
}

impl<'a> Drop for HaruPage<'a> {
    fn drop(&mut self) {

    }
}

impl<'a> PageAPI for HaruPage<'a> {
    type Error = crate::Error;
    type Document = &'a mut HaruDocument;
    fn new_page(document : Self::Document, rectangle : Rectangle) -> Result<Self, Self::Error> {
        if rectangle.right.is_none() || rectangle.bottom.is_none() {
            return Err("Rectangle cannot contain None".into());
        }
        let config : HaruConfig = Default::default();
        let page = unsafe {HPDF_AddPage(document.handle).denull()?};
        unsafe {
            let width : Dots = rectangle.width().ok_or(Error::from("Zero width page!"))?;
            let height : Dots = rectangle.height().ok_or(Error::from("Zero height page!"))?;
            HPDF_Page_SetWidth(page, width as HPDF_REAL).denull()?;
            HPDF_Page_SetHeight(page, height as HPDF_REAL).denull()?;
            let font_name = ffi::CString::new("Helvetica").unwrap();
            let font = HPDF_GetFont(document.handle, font_name.as_ptr(), ptr::null()).denull()?;
            HPDF_Page_SetTextLeading(page, config.text_leading).denull()?;
            HPDF_Page_SetFontAndSize(page, font, config.text_size).denull()?;
        }
        Ok(HaruPage { doc : document, offset: rectangle, page, config : Default::default()})
    }
    fn set_line(&mut self, color : &Color) -> Result<(), Self::Error> {
        Ok(())
    }
    fn set_fill(&mut self, color : &Color) -> Result<(), Self::Error> {
        Ok(())
    }
    fn draw_text<'b>(&mut self, text : &'b str, rectangle : &mut Rectangle) -> Result<&'b str, Self::Error> {
        let left = self.to_page_x(rectangle.left);
        let mut top = self.to_page_y(rectangle.top);
        let c_str = ffi::CString::new(text)?;
        let mut c_ptr = c_str.as_ptr();
        if let Some(right) = rectangle.right {
            let right = self.to_page_x(right);
            let mut t : HPDF_UINT = 0;
            let mut sum = 0;
            eprintln!("Printing Slice of len {}", text.len());
            while unsafe {*c_ptr > 0} {
                let bottom = top - self.config.text_leading;
                if let Some(rbottom) = rectangle.bottom {
                    if self.to_page_y(rbottom) > bottom {
                        return Ok(&text[sum as usize..]);
                    }
                }
                unsafe {
                    HPDF_Page_BeginText(self.page).denull()?;
                    let err = HPDF_Page_TextRect(self.page, left, top, right, bottom, c_ptr, HPDF_TALIGN_JUSTIFY, &mut t);
                    HPDF_Page_EndText(self.page).denull()?;
                    if err != HPDF_PAGE_INSUFFICIENT_SPACE as _{
                        err.denull()?;
                    }
                }

                if t == 0 {
                    return Err("Draw_text got into an infinite loop!".into());
                }
                eprintln!("Slice of len {} gets {}", text.len(), t);
                c_ptr = unsafe {c_ptr.offset(t as isize)};
                sum += t;
                t = 0;
                top -= self.config.text_leading;
            }
            if rectangle.bottom.is_none() {
                rectangle.bottom = Some(self.from_page_y(top));
            }
            Ok(text)
        } else {
            unsafe {
                HPDF_Page_BeginText(self.page).denull()?;
                HPDF_Page_MoveTextPos(self.page, left, top - self.config.text_size).denull()?;
                let err = HPDF_Page_ShowText(self.page, c_ptr);
                let right = self.from_page_x(HPDF_Page_GetCurrentTextPos(self.page).x);
                HPDF_Page_EndText(self.page).denull()?;
                err.denull()?;
                rectangle.right = Some(right);
                if rectangle.bottom.is_none() {
                    rectangle.bottom = Some(self.from_page_y(top - self.config.text_leading));
                }
            }
            Ok(&text[text.len()..])
        }
    }
    fn draw_rectangle(&mut self, rectangle : &Rectangle) -> Result<(), Self::Error> {
        let left = rectangle.left;
        let top = rectangle.top;
        match (rectangle.right, rectangle.bottom) {
            (Some(right), Some(bottom)) => {
                unsafe {
                    let (left, top, right, bottom) = (self.to_page_x(left), self.to_page_y(top), self.to_page_x(right), self.to_page_y(bottom));
                    HPDF_Page_Rectangle(self.page, left, bottom, right - left, top - bottom).denull()?;
                    HPDF_Page_Stroke(self.page).denull()?;
                }
                Ok(())
            },
            _ => Err("Rectangle not defined".into())
        }
    }
    fn draw_image(&mut self, rectangle : &Rectangle, image : &Image) -> Result<(), Self::Error> {
        let img = unsafe {HPDF_LoadPngImageFromMem(self.doc.handle, &image.image_data[0], image.image_data.len() as u32).denull()?};
        let left = self.to_page_x(rectangle.left);
        let top = self.to_page_y(rectangle.top);
        let right = rectangle.right.map(|t| self.to_page_x(t));
        let bottom = rectangle.bottom.map(|t| self.to_page_y(t));
        let ratio = unsafe {
            let width = HPDF_Image_GetWidth(img);
            let height = HPDF_Image_GetHeight(img);
            width as HPDF_REAL / height as HPDF_REAL
        };

        let (right, bottom) = match (right, bottom) {
            (Some(right), Some(bottom)) => (right, bottom),
            (Some(right), None) => (right, top - (right - left) / ratio),
            (None, Some(bottom)) => (left + (top - bottom) * ratio, bottom),
            _ => return Err("Rectangle not defined".into()),
        };
        unsafe {
            HPDF_Page_DrawImage(self.page, img, left, bottom, right - left, top - bottom).denull()?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
    }
}
