//
// shapes.rs
// Copyright (C) 2019 gerwin <gerwin@gerwin-linux>
// Distributed under terms of the MIT license.
//

pub type Dots = i32;

#[derive(Debug, Clone)]
pub struct Rectangle {
	pub left : Dots,
	pub top : Dots,
	pub right : Option<Dots>,
	pub bottom : Option<Dots>
}

impl Rectangle {
	pub fn new(x : Dots, y : Dots) -> Rectangle {
		Rectangle {
			left : x,
			right : None,
			top : y,
			bottom : None,
		}
	}

	pub fn with_width(self, w : Dots) -> Rectangle {
		Rectangle {
			right : Some(self.left + w),
			..self
		}
	}

	pub fn with_height(self, h : Dots) -> Rectangle {
		Rectangle {
			bottom : Some(self.top + h),
			..self
		}
	}

	pub fn margin(&self, margin : Dots) -> Rectangle {
		Rectangle {
			left : self.left + margin,
			right : self.right.map(|t| t - margin),
			top : self.top + margin,
			bottom : self.bottom.map(|t| t - margin),
		}
	}

	pub fn width(&self) -> Option<Dots> {
		Some(self.right? - self.left)
	}

	pub fn height(&self) -> Option<Dots> {
		Some(self.bottom? - self.top)
	}
}



#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn it_works() {
	}
}
