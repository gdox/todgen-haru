//
// basic.rs
// Copyright (C) 2019 gerwin <gerwin@gerwin-linux>
// Distributed under terms of the MIT license.
//

use todgen_haru::*;
use std::io::{self, Write};

fn main() -> Result<(), Error>{
    let mut document = HaruDocument::new()?;
    {
        let page_rect = Rectangle::new(0, 0).with_width(720).with_height(720);
        let mut page = HaruPage::new_page(&mut document, page_rect.clone())?;
        let image = Image::from_file("background.jpg")?;
        eprintln!("Image size: {}", image.image_data.len());
        page.draw_image(&page_rect, &image)?;
        let mut rectangle = Rectangle::new(60, 60).with_width(600).with_height(600);
        page.draw_rectangle(&rectangle)?;
        let mut rectangle = Rectangle::new(72, 72);

        for i in 1..10 {
            let text = "Hello world!";
            rectangle = rectangle.margin(10);
            page.draw_text(text, &mut rectangle)?;
            rectangle = rectangle.margin(-10);
            page.draw_rectangle(&rectangle)?;
            rectangle.top = rectangle.bottom.unwrap();
            rectangle.bottom = None;
            rectangle.left = rectangle.right.unwrap();
            rectangle.right = None;
        }


    }
    let pdf = document.save()?;
    
    io::stdout().write_all(&pdf)?;
    Ok(())
}


#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn it_works() {
	}
}
