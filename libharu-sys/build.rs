//
// build.rs
// Copyright (C) 2019 gerwin <gerwin@gerwin-linux>
// Distributed under terms of the MIT license.
//

fn main() {
    println!("cargo:rustc-link-search=native=/usr/local/lib/");
    println!("cargo:rustc-link-search=native=/usr/lib/x86_64-linux-gnu/");
    println!("cargo:rustc-link-lib=static=hpdf");
    println!("cargo:rustc-link-lib=z");
    println!("cargo:rustc-link-lib=png");
    println!("cargo:rerun-if-changed=src/");

}


#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn it_works() {
	}
}
